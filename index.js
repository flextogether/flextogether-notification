
//Originally built for Lambda but should be deployed to EC2

var moment = require('moment');
var moment = require('moment-timezone');
var AWS = require('aws-sdk');
AWS.config.update({
  accessKeyId: process.env.KEY,
  secretAccessKey: process.env.SECRET,
  region: 'us-east-1'
});
// Require and initialize outside of your main handler
const mysql = require('serverless-mysql')({
  config: {
    host     : process.env.ENDPOINT,
    database : process.env.DATABASE,
    user     : process.env.USERNAME,
    password : process.env.PASSWORD,
    timezone: 'UTC+0',
    dateStrings: [
      'DATE',
      'DATETIME'
    ]

    
  }
})

//============== Set the parameter - Remove for lambda
var flextrigger = 'linkNotice'; //reminder


// Main handler function
//===========================================Add for Lambda  
//exports.handler = async (event, context) => {
//===========================================Remove for Lambda
var nextClassId = null;
var nextClassStartTime = null;
var smile = '\u{1F603}';
async function timeCheck() {
  // Run your query
  /*
  var today = new Date();
var dateString = today.toISOString().substring(0, 10);
var timeString = today.toISOString().substring(12, 19);
console.log(dateString);
console.log(timeString);
*/
  var now = moment.utc();
  var nowF = now.format("YYYY-MM-DD hh:mm:ss");
  console.log("nowF = " + nowF);

  var future = now.add(30, 'minutes');
  var futureF = future.format("YYYY-MM-DD hh:mm:ss");
  console.log("futureF = " + futureF);




  let results = await mysql.query("SELECT ClassId, DateTime FROM Classes WHERE DateTime >= ? AND DateTime < ?", [nowF, futureF]);
  //nextClassStartTime = results[0].StartTime; ? ORDER BY DateTime;
  console.log("Between Query =" + results);
  // array does not exist, is not an array, or is empty
  console.log("line103 " + Array.isArray(results))
  console.log("line104 " + results.length)

  if (results.length == 0) {

    // Run clean up function
    mysql.end()
    process.exit()
  }
  else {
    nextClassId = results[0].ClassId;
    nextClassStartTime = moment.utc(results[0].DateTime).format();;
    console.log("next classID = " + nextClassId);
    console.log("next classStartTime = " + nextClassStartTime);

    main()
  }
}

async function main() {
  //Query Schedule table     
  let results1 = await mysql.query('SELECT PersonId, Buddy1 FROM CustomerClassReg WHERE classId = ?', [nextClassId]);
  console.log("main next class = " + nextClassId)
  personArray = [];
  buddyArray = [];
  for (var i = 0; i < results1.length; i++) {
    // var row = results1[i].PersonId;
    personArray.push(results1[i].PersonId);
    buddyArray.push(results1[i].Buddy1);
  }
  console.log(personArray);
  console.log(buddyArray);


  //Loop and Query Person
  for (var j = 0; j < personArray.length; j++) {
    console.log("line157:"+j +" personArray.length:" + personArray.length);

    var results2 = await mysql.query('SELECT FirstName, SMS, TimeZone FROM PersonInfo WHERE PersonId = ?', personArray[j]);
    personName = results2[0].FirstName;
    console.log('personName' + personName);
    personSMS = results2[0].SMS;
    console.log('personSMS' + personSMS);
    personTimeZone = results2[0].TimeZone;
    console.log('Time Zone P=' + personTimeZone)

    var results3 = await mysql.query('SELECT FirstName, SMS, TimeZone FROM PersonInfo WHERE PersonId = ?', buddyArray[j]);
    buddyName = results3[0].FirstName;
    console.log('buddyName' + buddyName);
    buddySMS = results3[0].SMS;
    console.log('buddySMS' + buddySMS);
    buddyTimeZone = results3[0].TimeZone;
    console.log('Time Zone B=' + buddyTimeZone)
    //Add Link
    FtLink = 'https://launch.flextogether.com/?room=' + personName;


    //Add to database prefernce for email or sms or both
    //IF preference = XXXY && SMS =!null && no stop THEN ...
    //Check sns if post can check for stop

    if (personTimeZone) {
      var m = moment.tz(nextClassStartTime, "UTC");
      var startTime = m.tz(personTimeZone).format("h:mm A z");
      console.log("startTIme = " + startTime);
      var sns = new AWS.SNS();
      console.log("line185 = " + flextrigger)
      if (flextrigger == 'reminder') {
        sns.publish({
          Message: 'Hi ' + personName + "! Friendly reminder, your next FlexTogether class with " + buddyName + " will begin at " + startTime + " " + smile,
          PhoneNumber: personSMS
        },
          function (err, data) {
            if (err) {
              console.log(err.stack);
              return;
            }
            
          });
      }

    
    if (flextrigger == "linkNotice") {
      // for linkNotice 
      sns.publish({
        Message: 'Hi ' + personName + "!" + " It's time to FlexTogether with " + buddyName + " Please click here: " + FtLink,
        PhoneNumber: personSMS
      }, function (err, data) {
        if (err) {
          console.log(err.stack);
          return;
        }
        
      });
    }
  }
      //duplicate if for Buddy

      if (buddyTimeZone) {
        var m = moment.tz(nextClassStartTime, "UTC");
        var startTime = m.tz(buddyTimeZone).format("h:mm A z");
        console.log("startTIme = " + startTime);
        var sns = new AWS.SNS();
        console.log("line222 = " + flextrigger)
        if (flextrigger == 'reminder') {
          sns.publish({
            Message: 'Hi ' + buddyName + "! Friendly reminder, your next FlexTogether class with " + personName + " will begin at " + startTime + " " + smile,
            PhoneNumber: buddySMS
          },
            function (err, data) {
              if (err) {
                console.log(err.stack);
                return;
              }
              
            });
        }
  
      
      if (flextrigger == "linkNotice") {
        sns.publish({
          Message: 'Hi ' + buddyName + "!" + " It's time to FlexTogether with " + personName + " Please click here: " + FtLink,
          PhoneNumber: buddySMS
        }, function (err, data) {
          //console.log("data"+data)
            if (err) console.log(err, err.stack); // an error occurred
           // else     console.log(data);           // successful response
        
          
        });
      
      }
    }


  }



  // Run clean up function
  await mysql.end()

  // Return the results
  //return results
}

//====================================================Remove for Lambda
timeCheck();
